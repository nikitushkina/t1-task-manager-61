package ru.t1.nikitushkina.tm.constant;

import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;

@UtilityClass
public final class UserTestData {

    @NotNull
    public final static String USER_TEST_LOGIN = "user01";

    @NotNull
    public final static String USER_TEST_PASSWORD = "user01pwd";

    @NotNull
    public final static String USER_TEST_LAST_NAME = "Звездунов";

    @NotNull
    public final static String USER_TEST_FIRST_NAME = "Андрей";

    @NotNull
    public final static String USER_TEST_MIDDLE_NAME = "Сергеевич";


}
