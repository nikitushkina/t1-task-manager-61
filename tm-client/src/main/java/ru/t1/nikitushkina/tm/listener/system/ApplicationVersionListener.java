package ru.t1.nikitushkina.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.nikitushkina.tm.dto.request.ServerVersionRequest;
import ru.t1.nikitushkina.tm.dto.response.ServerVersionResponse;
import ru.t1.nikitushkina.tm.event.ConsoleEvent;

@Component
public final class ApplicationVersionListener extends AbstractSystemListener {

    @NotNull
    public static final String NAME = "version";

    @NotNull
    public static final String DESCRIPTION = "Display application version.";

    @NotNull
    public static final String ARGUMENT = "-v";

    @Override
    @EventListener(condition = "@applicationVersionListener.getName() == #consoleEvent.name")
    public void execute(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[VERSION]");
        System.out.print("Client: ");
        System.out.println(propertyService.getApplicationVersion());
        @Nullable final ServerVersionRequest request = new ServerVersionRequest(getToken());
        @Nullable final ServerVersionResponse response = systemEndpoint.getVersion(request);
        System.out.println("Server: " + response.getVersion());
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
