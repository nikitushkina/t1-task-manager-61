package ru.t1.nikitushkina.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.nikitushkina.tm.dto.model.UserDTO;
import ru.t1.nikitushkina.tm.dto.request.UserProfileRequest;
import ru.t1.nikitushkina.tm.dto.response.UserProfileResponse;
import ru.t1.nikitushkina.tm.enumerated.Role;
import ru.t1.nikitushkina.tm.event.ConsoleEvent;

@Component
public final class UserViewProfileListener extends AbstractUserListener {

    @NotNull
    public static final String NAME = "view-user-profile";

    @NotNull
    public static final String DESCRIPTION = "View profile of current user.";

    @Override
    @EventListener(condition = "@userViewProfileListener.getName() == #consoleEvent.name")
    public void execute(@NotNull final ConsoleEvent consoleEvent) {
        @NotNull final UserProfileRequest request = new UserProfileRequest(getToken());
        @Nullable final UserProfileResponse response = authEndpointClient.profile(request);
        @NotNull final UserDTO user = response.getUser();
        showUser(user);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
