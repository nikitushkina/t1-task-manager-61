package ru.t1.nikitushkina.tm.constant;

import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.PropertySource;
import ru.t1.nikitushkina.tm.api.service.IPropertyService;
import ru.t1.nikitushkina.tm.dto.model.UserDTO;
import ru.t1.nikitushkina.tm.service.PropertyService;
import ru.t1.nikitushkina.tm.util.HashUtil;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;
import java.util.UUID;

@UtilityClass
@PropertySource("classpath:application.properties")
public final class UserTestData {

    @NotNull
    public final static String USER_TEST_LOGIN = "User 3";

    @NotNull
    public final static String USER_TEST_PASSWORD = "user3pwd";

    @NotNull
    public final static String USER_TEST_EMAIL = "u3@a.ru";

    @NotNull
    public final static String ADMIN_TEST_LOGIN = "User 4";

    @NotNull
    public final static String ADMIN_TEST_PASSWORD = "user4pwd";

    @NotNull
    public final static String ADMIN_TEST_EMAIL = "u4@a.ru";

    @NotNull
    public final static UserDTO USER_TEST = new UserDTO();

    @NotNull
    public final static UserDTO ADMIN_TEST = new UserDTO();

    @Nullable
    public final static UserDTO NULL_USER = null;

    @NotNull
    public final static String NON_EXISTING_USER_ID = UUID.randomUUID().toString();

    @NotNull
    public final static List<UserDTO> USER_LIST = Arrays.asList(USER_TEST, ADMIN_TEST);

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    static {
        @NotNull final Properties properties = new Properties();
        try {
            properties.load(ClassLoader.getSystemResourceAsStream("application.properties"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        @NotNull final String passwordSecret = properties.getProperty("password.secret");
        @NotNull final Integer passwordIteration = Integer.parseInt(properties.getProperty("password.iteration"));
        USER_TEST.setLogin(USER_TEST_LOGIN);
        USER_TEST.setPasswordHash(HashUtil.salt(USER_TEST_PASSWORD, passwordSecret, passwordIteration));
        USER_TEST.setEmail(USER_TEST_EMAIL);
        ADMIN_TEST.setLogin(ADMIN_TEST_LOGIN);
        ADMIN_TEST.setPasswordHash(HashUtil.salt(ADMIN_TEST_PASSWORD, passwordSecret, passwordIteration));
        ADMIN_TEST.setEmail(ADMIN_TEST_EMAIL);
    }


}
