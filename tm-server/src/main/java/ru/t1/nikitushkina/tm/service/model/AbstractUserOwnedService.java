package ru.t1.nikitushkina.tm.service.model;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.nikitushkina.tm.api.service.model.IUserOwnedService;
import ru.t1.nikitushkina.tm.api.service.model.IUserService;
import ru.t1.nikitushkina.tm.exception.user.UserIdEmptyException;
import ru.t1.nikitushkina.tm.model.AbstractUserOwnedModel;
import ru.t1.nikitushkina.tm.repository.model.AbstractUserOwnedRepository;

@Service
@NoArgsConstructor
public abstract class AbstractUserOwnedService<M extends AbstractUserOwnedModel>
        extends AbstractService<M> implements IUserOwnedService<M> {

    @NotNull
    @Autowired
    private IUserService userService;

    @NotNull
    @Override
    @Transactional
    public M add(@Nullable final String userId, @NotNull final M model) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        model.setUser(userService.findOneById(userId));
        return getRepository().save(model);
    }

    @Nullable
    protected abstract AbstractUserOwnedRepository<M> getRepository();

    @Override
    @Transactional
    public void remove(@Nullable final String userId, @Nullable final M model) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) return;
        if (findOneById(model.getUser().getId(), model.getId()) == null) return;
        getRepository().delete(model);
    }

    @Override
    @Transactional
    public void update(@Nullable final String userId, @Nullable final M model) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) return;
        if (findOneById(model.getUser().getId(), model.getId()) == null) return;
        model.setUser(userService.findOneById(userId));
        getRepository().save(model);
    }

}
