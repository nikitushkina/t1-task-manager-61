package ru.t1.nikitushkina.tm.api.service;

import liquibase.Liquibase;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public interface IAdminService {

    void dropScheme(@Nullable String initToken) throws Exception;

    @NotNull
    Liquibase getLiquibase();

    void initScheme(@Nullable String initToken) throws Exception;

}
