package ru.t1.nikitushkina.tm;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.t1.nikitushkina.tm.component.Bootstrap;
import ru.t1.nikitushkina.tm.configuration.LoggerConfiguration;

public final class Application {

    public static final String QUEUE = "LOGGER";

    @SneakyThrows
    public static void main(String[] args) {
        @NotNull final ApplicationContext context = new AnnotationConfigApplicationContext(LoggerConfiguration.class);
        @NotNull final Bootstrap bootstrap = context.getBean(Bootstrap.class);
        bootstrap.startLogger();
    }

}
