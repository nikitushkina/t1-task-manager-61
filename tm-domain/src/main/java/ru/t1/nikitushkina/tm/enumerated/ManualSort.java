package ru.t1.nikitushkina.tm.enumerated;

import lombok.Getter;
import org.jetbrains.annotations.Nullable;
import ru.t1.nikitushkina.tm.comparator.CreatedComparator;
import ru.t1.nikitushkina.tm.comparator.NameComparator;
import ru.t1.nikitushkina.tm.comparator.StatusComparator;

import java.util.Comparator;

@Getter
public enum ManualSort {

    BY_NAME("Sort by name", NameComparator.INSTANCE),
    BY_STATUS("Sort by status", StatusComparator.INSTANCE),
    BY_CREATED("Sort by created date", CreatedComparator.INSTANCE);

    @Nullable
    private final String displayName;

    @Nullable
    private final Comparator<?> comparator;

    ManualSort(@Nullable final String displayName, @Nullable final Comparator<?> comparator) {
        this.displayName = displayName;
        this.comparator = comparator;
    }

    @Nullable
    public static ManualSort toSort(@Nullable final String value) {
        if (value == null || value.isEmpty()) return null;
        for (final ManualSort manualSort : values()) {
            if (manualSort.name().equals(value)) return manualSort;
        }
        return null;
    }

    @SuppressWarnings("rawtypes")
    public Comparator getComparator() {
        return comparator;
    }

}
